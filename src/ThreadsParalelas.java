
public class ThreadsParalelas {

	public static void main(String[] args) {
		MinhaThread thread1 = new MinhaThread(1);		
		MinhaThread thread2 = new MinhaThread(2);
		MinhaThreadRunnable runnable1 = new MinhaThreadRunnable(5);
		MinhaThreadRunnable runnable2 = new MinhaThreadRunnable(6);
		
		int count = Thread.activeCount();
		System.out.println("Número de Threads ativas = " + count);		
		thread1.start();
		thread2.start();

		new Thread(runnable1).start();
		new Thread(runnable2).start();
		count = Thread.activeCount();
		System.out.println("Número de Threads ativas  = " + count);
		int contador = 10;
		for(int i = 0; i < contador; i++) {
			System.out.println("Main: contador = " + i);
		}
	}

}
